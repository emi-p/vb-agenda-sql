﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.EjemploYoutubeDataSet = New ejemplo2Datos.ejemploYoutubeDataSet()
        Me.btnDetallesCitas = New System.Windows.Forms.Button()
        Me.btnContactos = New System.Windows.Forms.Button()
        Me.ContactosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ContactosTableAdapter = New ejemplo2Datos.ejemploYoutubeDataSetTableAdapters.contactosTableAdapter()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ContactosBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.IdPersonaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ApellidoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmpresaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EjemploYoutubeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ContactosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ContactosBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BindingSource1
        '
        Me.BindingSource1.DataSource = Me.EjemploYoutubeDataSet
        Me.BindingSource1.Position = 0
        '
        'EjemploYoutubeDataSet
        '
        Me.EjemploYoutubeDataSet.DataSetName = "ejemploYoutubeDataSet"
        Me.EjemploYoutubeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnDetallesCitas
        '
        Me.btnDetallesCitas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDetallesCitas.Location = New System.Drawing.Point(12, 205)
        Me.btnDetallesCitas.Name = "btnDetallesCitas"
        Me.btnDetallesCitas.Size = New System.Drawing.Size(83, 33)
        Me.btnDetallesCitas.TabIndex = 0
        Me.btnDetallesCitas.Text = "Citas"
        Me.btnDetallesCitas.UseVisualStyleBackColor = True
        '
        'btnContactos
        '
        Me.btnContactos.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContactos.Location = New System.Drawing.Point(114, 205)
        Me.btnContactos.Name = "btnContactos"
        Me.btnContactos.Size = New System.Drawing.Size(96, 33)
        Me.btnContactos.TabIndex = 1
        Me.btnContactos.Text = "Contactos"
        Me.btnContactos.UseVisualStyleBackColor = True
        '
        'ContactosBindingSource
        '
        Me.ContactosBindingSource.DataMember = "contactos"
        Me.ContactosBindingSource.DataSource = Me.BindingSource1
        '
        'ContactosTableAdapter
        '
        Me.ContactosTableAdapter.ClearBeforeFill = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdPersonaDataGridViewTextBoxColumn, Me.ApellidoDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.EmpresaDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.ContactosBindingSource1
        Me.DataGridView1.Location = New System.Drawing.Point(12, 12)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(457, 150)
        Me.DataGridView1.TabIndex = 2
        '
        'ContactosBindingSource1
        '
        Me.ContactosBindingSource1.DataMember = "contactos"
        Me.ContactosBindingSource1.DataSource = Me.BindingSource1
        '
        'IdPersonaDataGridViewTextBoxColumn
        '
        Me.IdPersonaDataGridViewTextBoxColumn.DataPropertyName = "IdPersona"
        Me.IdPersonaDataGridViewTextBoxColumn.HeaderText = "IdPersona"
        Me.IdPersonaDataGridViewTextBoxColumn.Name = "IdPersonaDataGridViewTextBoxColumn"
        Me.IdPersonaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ApellidoDataGridViewTextBoxColumn
        '
        Me.ApellidoDataGridViewTextBoxColumn.DataPropertyName = "Apellido"
        Me.ApellidoDataGridViewTextBoxColumn.HeaderText = "Apellido"
        Me.ApellidoDataGridViewTextBoxColumn.Name = "ApellidoDataGridViewTextBoxColumn"
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        '
        'EmpresaDataGridViewTextBoxColumn
        '
        Me.EmpresaDataGridViewTextBoxColumn.DataPropertyName = "Empresa"
        Me.EmpresaDataGridViewTextBoxColumn.HeaderText = "Empresa"
        Me.EmpresaDataGridViewTextBoxColumn.Name = "EmpresaDataGridViewTextBoxColumn"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(481, 250)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.btnContactos)
        Me.Controls.Add(Me.btnDetallesCitas)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EjemploYoutubeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ContactosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ContactosBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents EjemploYoutubeDataSet As ejemploYoutubeDataSet
    Friend WithEvents btnDetallesCitas As Button
    Friend WithEvents btnContactos As Button
    Friend WithEvents ContactosBindingSource As BindingSource
    Friend WithEvents ContactosTableAdapter As ejemploYoutubeDataSetTableAdapters.contactosTableAdapter
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents IdPersonaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ApellidoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EmpresaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ContactosBindingSource1 As BindingSource
End Class
